package com.yonyou.mall.service.catalog.handler;

import com.yonyou.mall.service.catalog.common.event.ProductCreatedEvent;
import com.yonyou.mall.service.catalog.common.event.ProductReserveRollbackedEvent;
import com.yonyou.mall.service.catalog.common.event.ProductReservedEvent;
import com.yonyou.mall.service.catalog.domain.Product;
import com.yonyou.mall.service.catalog.repository.ProductRepository;
import org.axonframework.eventhandling.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2017/3/30.
 */
@Component
public class ProductEventHandler {
    private final Logger log = LoggerFactory.getLogger(ProductEventHandler.class);

    private final ProductRepository productRepository;

    public ProductEventHandler(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @EventHandler
    public void handle(ProductCreatedEvent event) {
        Product product = new Product();
        product.setCode(event.getCode());
        product.setName(event.getName());
        product.setPrice(event.getPrice());
        product.setInventory(event.getInventory());
        product.setDescription(event.getDescription());
        productRepository.save(product);

        log.info("Product {} is created", product);
    }

    @EventHandler
    public void handle(ProductReservedEvent event) {
        Product product = productRepository.findByCode(event.getProductCode());
        if (product == null) {
            log.error("Cannot find product with code {}", event.getProductCode());
            return;
        }

        product.setInventory(product.getInventory().subtract(event.getQuantity()));
        productRepository.save(product);
    }

    @EventHandler
    public void handle(ProductReserveRollbackedEvent event) {
        Product product = productRepository.findByCode(event.getProductCode());
        if (product == null) {
            log.error("Cannot find product with code {}", event.getProductCode());
            return;
        }

        product.setInventory(product.getInventory().add(event.getQuantity()));
        productRepository.save(product);
    }
}
